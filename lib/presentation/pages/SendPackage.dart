import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:training_final_2/presentation/widgets/CustomTextField.dart';

class SendPackage extends StatefulWidget {
  const SendPackage({super.key});

  @override
  State<SendPackage> createState() => _SendPackageState();
}

class _SendPackageState extends State<SendPackage> {
  @override
  Widget build(BuildContext context) {
    var pointAddress = TextEditingController();
    var pointCountry = TextEditingController();
    var pointPhone = TextEditingController();
    var pointOthers = TextEditingController();
    var destinationAddress = TextEditingController();
    var destinationCountry = TextEditingController();
    var destinationPhone = TextEditingController();
    var destinationOthers = TextEditingController();
    var packageItem = TextEditingController();
    var packageWeight = TextEditingController();
    var packageWorth = TextEditingController();

    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: Column(
        children: [
          SizedBox(
            height: 108,
            width: double.infinity,
            child: Column(
              children: [
                SizedBox(
                  height: 106,
                  width: double.infinity,
                  child: Column(
                    children: [
                      SizedBox(height: 73,),
                      Row(
                        children: [
                          SizedBox(width: 15),
                          GestureDetector(
                            onTap: (){
                              Navigator.of(context).pop();
                            },
                            child: Image.asset('assets/back.png'),
                          ),
                          const SizedBox(width: 99),
                          Text(
                            'Send a package',
                            style: Theme.of(context).textTheme.titleMedium?.copyWith(fontSize: 16),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 2,
                  decoration: const BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Color.fromARGB(38, 0, 0, 0),
                          blurRadius: 2,
                          offset: Offset(0, 2),
                        )
                      ]
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal:  24),
            child: Column(
              children: [
                SingleChildScrollView(
                  child: Column(
                    children: [
                      const SizedBox(height: 43),
                      Row(
                        children: [
                          Image.asset('assets/point.png'),
                          const SizedBox(width: 8),
                          Text(
                              'Origin Details',
                            style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 14),
                          )
                        ],
                      ),
                      CustomTextField(
                          hint: 'Address',
                          controller: pointAddress
                      ),
                      CustomTextField(
                          hint: 'State,Country',
                          controller: pointCountry
                      ),
                      CustomTextField(
                          hint: 'Phone number',
                          controller: pointPhone
                      ),
                      CustomTextField(
                          hint: 'Others',
                          controller: pointOthers
                      ),
                      const SizedBox(height: 39),
                      Row(
                        children: [
                          Image.asset('assets/point2.png'),
                          const SizedBox(width: 8),
                          Text(
                            'Destination Details',
                            style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 14),
                          )
                        ],
                      ),
                      CustomTextField(
                          hint: 'Address',
                          controller: destinationAddress
                      ),
                      CustomTextField(
                          hint: 'State,Country',
                          controller: destinationCountry
                      ),
                      CustomTextField(
                          hint: 'Phone number',
                          controller: destinationPhone
                      ),
                      CustomTextField(
                          hint: 'Others',
                          controller: destinationOthers
                      ),
                      const SizedBox(height: 11),
                      Row(
                        children: [
                          Image.asset('assets/add.png'),
                          const SizedBox(width: 6),
                          const Text(
                            'Add destination',
                            style: TextStyle(
                              color: Color.fromARGB(255, 167, 167, 167),
                              fontSize: 12,
                              fontWeight: FontWeight.w400
                            ),
                          )
                        ],
                      ),
                      const SizedBox(height: 13),
                      Row(
                        children: [
                          Text(
                            'Package Details',
                            style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 14),
                          ),
                        ],
                      ),
                      CustomTextField(
                          hint: 'package items',
                          controller: packageItem
                      ),
                      CustomTextField(
                          hint: 'Weight of item(kg)',
                          controller: packageWeight
                      ),
                      CustomTextField(
                          hint: 'Worth of Items',
                          controller: packageWorth
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 39),
                Row(
                  children: [
                    Text(
                        'Select delivery type',
                      style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 14),
                    ),
                  ],
                ),
                SizedBox(height: 16),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 159,
                      height: 75,
                      decoration: const BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromARGB(38, 0, 0, 0),
                              blurRadius: 2,
                              offset: Offset(0, 2),
                            )]
                      ),
                      child: Expanded(
                          child: FilledButton(
                            style: FilledButton.styleFrom(
                              backgroundColor: Color.fromARGB(255, 5, 96, 250),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)
                              )
                            ),
                              onPressed: (){},
                              child: Column(
                                children: [
                                  SizedBox(height: 13,),
                                  Image.asset('assets/clock.png'),
                                  SizedBox(height: 10),
                                  Text(
                                    'Instant delivery',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14
                                    ),
                                  )
                                ],
                              )
                          )
                      ),
                    ),
                    SizedBox(width: 24),
                    Container(
                      width: 159,
                      height: 75,
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromARGB(38, 0, 0, 0),
                              blurRadius: 2,
                              offset: Offset(0, 2),
                            )]
                      ),
                      child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                          side: BorderSide(color: Colors.white),
                            backgroundColor: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)
                            )
                        ),
                          onPressed: (){},
                          child: Column(
                            children: [
                              SizedBox(height: 13,),
                              Image.asset('assets/time.png'),
                              SizedBox(height: 10),
                              Text(
                                'Scheduled delivery',
                                style: TextStyle(
                                    color: Color.fromARGB(255, 167, 167, 167),
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12
                                ),
                              )
                            ],
                          )
                      ),
                    )
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}