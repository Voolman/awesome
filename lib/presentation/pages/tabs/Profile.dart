import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:training_final_2/presentation/pages/AddPayment.dart';
import 'package:training_final_2/presentation/pages/Notification.dart';
import 'package:training_final_2/presentation/pages/SendPackage.dart';

import '../../widgets/CustomListTile.dart';

class Profile extends StatefulWidget {
  const Profile({super.key});

  @override
  State<Profile> createState() => _ProfileState();
}
bool isSee = true;
bool isDark = false;
class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          SizedBox(
            height: 108,
            width: double.infinity,
            child: Column(
              children: [
                const SizedBox(height: 73),
                Center(
                  child: Text(
                    'Profile',
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(fontSize: 16),
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 2,
            decoration: const BoxDecoration(
              boxShadow: [
                BoxShadow(
                color: Color.fromARGB(38, 0, 0, 0),
                blurRadius: 2,
                offset: Offset(0, 2),
              )]
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 17, left: 24, right: 24),
            child: Column(
              children: [
                SizedBox(
                  height: 75,
                  child: ListTile(
                    contentPadding: const EdgeInsets.symmetric(vertical: 10),
                    leading: Container(
                        width: 60,
                        height: 60,
                        decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            color: Color.fromARGB(255, 207, 207, 207)
                        ),
                        child: const Icon(Icons.person, color: Color.fromARGB(255, 58, 58, 58))
                    ),
                    title: const Text(
                      "Hello Ken",
                      style: TextStyle(
                          color: Color.fromARGB(255, 58, 58, 58),
                          fontSize: 16,
                          fontFamily: "Roboto",
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500
                      ),
                    ),
                    subtitle: RichText(
                        text: TextSpan(
                            children: [
                              const TextSpan(
                                  text: "Current balance: ",
                                  style: TextStyle(
                                      color: Color.fromARGB(255, 58, 58, 58),
                                      fontSize: 12,
                                      fontFamily: "Roboto",
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w400
                                  )
                              ),
                              TextSpan(
                                  text: (isSee) ? "N10,712:00" : "*"*8,
                                  style: const TextStyle(
                                      color: Color.fromARGB(255, 5, 96, 250),
                                      fontSize: 12,
                                      fontFamily: "Roboto",
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w500
                                  )
                              )
                            ]
                        ),
                    ),
                    trailing: GestureDetector(
                      onTap: (){
                        setState(() {
                          isSee = !isSee;
                        });
                      },
                        child: Image.asset('assets/eye-slash.png')
                    ),
                  ),
                ),
                const SizedBox(height: 19),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                        'Enable dark Mode',
                      style: TextStyle(
                        color: Color.fromARGB(255, 58, 58, 58),
                        fontSize: 16,
                        fontWeight: FontWeight.w500
                      ),
                    ),
                    Switch(
                      inactiveThumbColor: Colors.white,
                        inactiveTrackColor: const Color.fromARGB(255, 215, 215, 215),
                        value: isDark,
                        onChanged: (value){
                          setState(() {
                            isDark = value;
                          });
                        }
                    )
                  ],
                ),
                CustomListTile(
                    color: const Color.fromARGB(255, 58, 58, 58),
                    icon: Image.asset('assets/profile_page.png'),
                    label: "Edit Profile",
                    text: 'Name, phone no, address, email ...'
                ),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => const SendPackage()));
                  },
                  child: CustomListTile(
                      icon: Image.asset('assets/paper.png'),
                      color: const Color.fromARGB(255, 58, 58, 58),
                      label: "Statements & Reports",
                      text: 'Download transaction details, orders, deliveries'
                  ),
                ),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => const NotificationPage()));
                  },
                  child: CustomListTile(
                      icon: Image.asset('assets/notification.png'),
                      color: const Color.fromARGB(255, 58, 58, 58),
                      label: "Notification Settings",
                      text: 'mute, unmute, set location & tracking setting'
                  ),
                ),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => const AddPaymentPage()));
                  },
                  child: CustomListTile(
                      icon: Image.asset('assets/wallet.png'),
                      color: const Color.fromARGB(255, 58, 58, 58),
                      label: "Card & Bank account settings",
                      text: 'change cards, delete card details'
                  ),
                ),
                CustomListTile(
                    icon: Image.asset('assets/two-person.png'),
                    color: const Color.fromARGB(255, 58, 58, 58),
                    label: "Referrals",
                    text: 'check no of friends and earn'
                ),
                CustomListTile(
                    icon: Image.asset('assets/map.png'),
                    color: const Color.fromARGB(255, 58, 58, 58),
                    label: "About Us",
                    text: 'know more about us, terms and conditions'
                ),
                GestureDetector(
                  onTap: (){
                    exit(0);
                  },
                  child: CustomListTile(
                    icon: Image.asset('assets/log-out.png'),
                    color: const Color.fromARGB(255, 237, 58, 58),
                    label: "Log Out",
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}