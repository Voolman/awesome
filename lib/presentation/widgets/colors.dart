import 'package:flutter/material.dart';

abstract class ColorsApp{
  abstract final Color text;
  abstract final Color accent;
  abstract final Color textAccent;
  abstract final Color disableAccent;
  abstract final Color disableTextAccent;
  abstract final Color hint;
  abstract final Color error;
  abstract final Color block;
  abstract final Color background;
  abstract final Color iconTint;
  abstract final Color subtext;
}

class LightColorsApp extends ColorsApp{
  @override
  // TODO: implement accent
  Color get accent => Color.fromARGB(255, 117, 118, 214);

  @override
  // TODO: implement background
  Color get background => Color.fromARGB(255, 255, 250, 240);

  @override
  // TODO: implement block
  Color get block => Color.fromARGB(255, 196, 200, 203);

  @override
  // TODO: implement disableAccent
  Color get disableAccent => Color.fromARGB(255, 167, 167, 167);

  @override
  // TODO: implement disableTextAccent
  Color get disableTextAccent => Color.fromARGB(255, 255, 255, 255);

  @override
  // TODO: implement error
  Color get error => Color.fromARGB(255, 202, 0, 0);

  @override
  // TODO: implement hint
  Color get hint => Color.fromARGB(255, 207, 207, 207);

  @override
  // TODO: implement iconTint
  Color get iconTint => Color.fromARGB(255, 20, 20, 20);

  @override
  // TODO: implement subtext
  Color get subtext => Color.fromARGB(255, 92, 99, 106);

  @override
  // TODO: implement text
  Color get text => Color.fromARGB(255, 58, 58, 58);

  @override
  // TODO: implement textAccent
  Color get textAccent => Color.fromARGB(255, 255, 255, 255);
}



class DarkColorsApp extends ColorsApp{
  @override
  // TODO: implement accent
  Color get accent => Color.fromARGB(255, 11, 94, 215);

  @override
  // TODO: implement background
  Color get background => Color.fromARGB(255, 26, 26, 26);

  @override
  // TODO: implement block
  Color get block => Color.fromARGB(255, 68, 70, 72);

  @override
  // TODO: implement disableAccent
  Color get disableAccent => Color.fromARGB(255, 87, 87, 87);

  @override
  // TODO: implement disableTextAccent
  Color get disableTextAccent => Color.fromARGB(255, 255, 255, 255);

  @override
  // TODO: implement error
  Color get error => Color.fromARGB(255, 202, 0, 0);

  @override
  // TODO: implement hint
  Color get hint => Color.fromARGB(255, 87, 87, 87);

  @override
  // TODO: implement iconTint
  Color get iconTint => Color.fromARGB(255, 255, 255, 255);

  @override
  // TODO: implement subtext
  Color get subtext => Color.fromARGB(255, 88, 18, 18);

  @override
  // TODO: implement text
  Color get text => Color.fromARGB(255, 255, 255, 255);

  @override
  // TODO: implement textAccent
  Color get textAccent => Color.fromARGB(255, 255, 255, 255);
}