import 'package:flutter/material.dart';

class AddPaymentPage extends StatefulWidget {
  const AddPaymentPage({super.key});

  @override
  State<AddPaymentPage> createState() => _AddPaymentPageState();
}

class _AddPaymentPageState extends State<AddPaymentPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
          children: [
            SizedBox(
              height: 108,
              width: double.infinity,
              child: Column(
                children: [
                  SizedBox(height: 73,),
                  Row(
                    children: [
                      SizedBox(width: 15),
                      GestureDetector(
                        onTap: (){
                          Navigator.of(context).pop();
                        },
                        child: Image.asset('assets/back.png'),
                      ),
                      const SizedBox(width: 85),
                      Text(
                        'Add Payment method',
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(fontSize: 16),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              height: 2,
              decoration: const BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Color.fromARGB(38, 0, 0, 0),
                      blurRadius: 2,
                      offset: Offset(0, 2),
                    )]
              ),
            ),
          ]
      ),
    );
  }
}